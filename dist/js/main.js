class VerticalMouseDrivenCarousel {
	constructor(options = {}) {
		const _defaults = {
			carousel: ".js-carousel",
			bgImg: ".js-carousel-bg-img",
			list: ".js-carousel-list",
			listItem: ".js-carousel-list-item"
		};

		this.posY = 0;

		this.defaults = Object.assign({}, _defaults, options);

		this.initCursor();
		this.init();
		this.bgImgController();
	}

	//region getters
	getBgImgs() {
		return document.querySelectorAll(this.defaults.bgImg);
	}

	getListItems() {
		return document.querySelectorAll(this.defaults.listItem);
	}

	getList() {
		return document.querySelector(this.defaults.list);
	}

	getCarousel() {
		return document.querySelector(this.defaults.carousel);
	}

	init() {
		TweenMax.set(this.getBgImgs(), {
			autoAlpha: 0,
			scale: 1.05
		});

		TweenMax.set(this.getBgImgs()[0], {
			autoAlpha: 1,
			scale: 1
		});

		this.listItems = this.getListItems().length - 1;
		
		this.listOpacityController(0);
	}

	// initCursor() {
	// 	const listHeight = this.getList().clientHeight;
	// 	const carouselHeight = this.getCarousel().clientHeight;

	// 	this.getCarousel().addEventListener(
	// 		"mousemove",
	// 		event => {
	// 			this.posY = event.pageY - this.getCarousel().offsetTop;
	// 			let offset = -this.posY / carouselHeight * listHeight;

	// 			TweenMax.to(this.getList(), 0.3, {
	// 				y: offset,
	// 				ease: Power4.easeOut
	// 			});
	// 		},
	// 		false
	// 	);
	// }

	bgImgController() {
		for (const link of this.getListItems()) {
			link.addEventListener("mouseenter", ev => {
				let currentId = ev.currentTarget.dataset.itemId;

				this.listOpacityController(currentId);

				TweenMax.to(ev.currentTarget, 0.3, {
					autoAlpha: 1
				});

				 

				TweenMax.to(this.getBgImgs()[currentId], 0.6, {
					autoAlpha: 1,
					scale: 1
				});
			});
		}
	}

	listOpacityController(id) {
		id = parseInt(id);
		let aboveCurrent = this.listItems - id;
		let belowCurrent = parseInt(id);

		if (aboveCurrent > 0) {
			for (let i = 1; i <= aboveCurrent; i++) {
				let opacity = 0.5 / i;
				let offset = 5 * i;
				TweenMax.to(this.getListItems()[id + i], 0.5, {
					autoAlpha: opacity,
					x: offset,
					ease: Power3.easeOut
				});
			}
		}

		if (belowCurrent > 0) {
			for (let i = 0; i <= belowCurrent; i++) {
				let opacity = 0.5 / i;
				let offset = 5 * i;
				TweenMax.to(this.getListItems()[id - i], 0.5, {
					autoAlpha: opacity,
					x: offset,
					ease: Power3.easeOut
				});
			}
		}
	}
}

new VerticalMouseDrivenCarousel();


function openmenu(){
	console.log('sdvs');
	const menue = document.querySelector('.mobile-menu-list'); 
	var status = document.getElementById('menuBar').getAttribute('status');
 	if (status === 'false') {
		menue.style.right =  0; 
		document.getElementById('menuBar').setAttribute('status','true'); 
		document.getElementById('menuBar').setAttribute('src','/dist/images/icons/menue_icon_x.png'); 
	}else{
		menue.style.right =  60+'%'; 
		document.getElementById('menuBar').setAttribute('status','false');
		document.getElementById('menuBar').setAttribute('src','/dist/images/icons/menue_icon.png'); 
	}
	  


}

function sleep(ms){  
	return new Promise(resolve => setTimeout(resolve, ms));
	}
	async function demo() {
	 
	 var text  = ['create','devolop','innovate','design'];

	// Sleep in loop
	for (let i = 0; i < 4; i++) {
		 
		await sleep(4000);
		document.getElementById('coute').innerText="";
		document.getElementById('coute').innerText=text[i];
		// console.log(text[i]);

		if(i === 3){
		i = 0
	}
	} 
	

	}
	 

demo();